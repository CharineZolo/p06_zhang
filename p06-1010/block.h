//
//  block.h
//  p06-1010
//
//  Created by 米拉 on 4/15/17.
//  Copyright © 2017 米拉. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface block : NSObject{
    int block_num;
    int block_color;
    NSMutableArray *block_rotation;
}

@property(nonatomic,assign) int block_num;
@property(nonatomic,assign) int block_color;
@property(nonatomic,copy) NSMutableArray *block_rotation;

@end
