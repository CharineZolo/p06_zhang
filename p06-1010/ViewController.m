//
//  ViewController.m
//  p06-1010
//
//  Created by 米拉 on 4/6/17.
//  Copyright © 2017 米拉. All rights reserved.
//

#import "ViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "blocks.h"
#import <AVFoundation/AVFoundation.h>


@interface ViewController(){
    AVAudioPlayer *_audioPlayer;
    AVAudioPlayer *_groudaudio;
    AVAudioPlayer * _rotateaudio;
    AVAudioPlayer * _removeaudio;
    AVAudioPlayer * _gameoveraudio;
    AVAudioPlayer * _dropAudio;
    
    
}
@end

@implementation ViewController

@synthesize allblock;
@synthesize oneblock;
@synthesize speed,playagain,NDMode,GameImageV,highscore;
//@synthesize gameover;
//@synthesize play
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    //gameover.hidden =true;
    //playagain.hidden=false;
    NDMode.tag=0;
    pausebtn.tag=0;
    
    [GameImageV setBackgroundColor:[UIColor whiteColor]];
    UIImage *image = [UIImage imageNamed: @"bg.png"];
    [GameImageV setImage:image];
    
    playagain.layer.cornerRadius = 10; // this value vary as per your desire
    playagain.clipsToBounds = YES;
    
    pausebtn.layer.cornerRadius = 10; // this value vary as per your desire
    pausebtn.clipsToBounds = YES;
    
    NDMode.layer.cornerRadius = 10; // this value vary as per your desire
    NDMode.clipsToBounds = YES;
    UIImage *btnImage = [UIImage imageNamed:@"night.png"];
    [NDMode setBackgroundImage:btnImage forState:UIControlStateNormal];
    highscore.tag=0;
    label.tag=0;
    
    allblock = [[blocks alloc]init];
    [allblock build_blocks];//
    
    oneblock = [[block alloc]init];
    
    completenum = 0;
    label.text = [NSString stringWithFormat:@"%d",completenum*100];
    
    highscore.text = [NSString stringWithFormat:@"%d",completenum*100];
    
  ///-----------------------add sound--------------------
    NSString *path = [NSString stringWithFormat:@"%@/pause.mp3", [[NSBundle mainBundle] resourcePath]];
    NSURL *soundUrl = [NSURL fileURLWithPath:path];
    
    
    // Create audio player object and initialize with URL to sound
    _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
    
    
    NSString *path2 = [NSString stringWithFormat:@"%@/start.mp3", [[NSBundle mainBundle] resourcePath]];
    NSURL *soundUrl2 = [NSURL fileURLWithPath:path2];
    
    [_audioPlayer play];
    // Create audio player object and initialize with URL to sound
    _groudaudio = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl2 error:nil];
    
    NSString *path3 = [NSString stringWithFormat:@"%@/block-rotate.mp3", [[NSBundle mainBundle] resourcePath]];
    NSURL *soundUrl3 = [NSURL fileURLWithPath:path3];
    
    
    // Create audio player object and initialize with URL to sound
    _rotateaudio = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl3 error:nil];
    
    NSString *path4 = [NSString stringWithFormat:@"%@/line-removal4.mp3", [[NSBundle mainBundle] resourcePath]];
    NSURL *soundUrl4 = [NSURL fileURLWithPath:path4];
    _removeaudio = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl4 error:nil];

    
    NSString *path5 = [NSString stringWithFormat:@"%@/gameover.mp3", [[NSBundle mainBundle] resourcePath]];
    NSURL *soundUrl5 = [NSURL fileURLWithPath:path5];
    
    
    // Create audio player object and initialize with URL to sound
    _gameoveraudio = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl5 error:nil];
    NSString *path6 = [NSString stringWithFormat:@"%@/block-rotate.mp3", [[NSBundle mainBundle] resourcePath]];
    NSURL *soundUrl6 = [NSURL fileURLWithPath:path6];
    
    
    // Create audio player object and initialize with URL to sound
    _dropAudio = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl6 error:nil];
    // Create audio player object and initialize with URL to sound
    ///==================================================
    
    
    blockpicarr = [[NSArray alloc]initWithObjects:@"1.png",@"2.png",@"3.png",@"4.png",@"5.png",@"6.png",@"7.png", nil];
    kindnum = [blockpicarr count];
    
    sizex = 12;
    sizey = 23;
    wholearr = [[NSMutableArray alloc]initWithCapacity:sizey];//
    for (i = 0; i < sizey; i++) {
        [wholearr addObject:[[NSMutableArray alloc]initWithCapacity:10]];
        for (j = 0; j < sizex; j++) {
            [[wholearr objectAtIndex:i] addObject:@"0"];
        }
    }
    blocksize = 20;
    
    nowblock = (arc4random() % (kindnum-1)) + 0;
    maxnum = 4;
    nextshapearr = [[NSMutableArray alloc] initWithCapacity:4];
    for (k = 0; k < maxnum; k++) {
        imgview = [[UIImageView alloc]init];
        [nextshapearr addObject:imgview];
        [nextblockview addSubview:[nextshapearr objectAtIndex:k]];
    }
    startx = 5;
    starty = 5;
  
    oneshapearr = [[NSMutableArray alloc] initWithCapacity:4];
    [self performSelector:@selector(randomgetblocks)];
    
    speed = 0.9;
    timer = [NSTimer scheduledTimerWithTimeInterval:speed target:self selector:@selector(moveblocks) userInfo:Nil repeats:YES];
    istop = NO;
}
- (IBAction)changeNightmode:(id)sender {
    if((NDMode.tag==0)){
        self.view.backgroundColor = [UIColor blackColor];
//imgview
        UIImage *image = [UIImage imageNamed: @"nbg.png"];
        //[GameImageV setBackgroundColor:[UIColor blackColor]];
        [GameImageV setImage:image];

        NDMode.tag=1;
        
        UIImage *btnImage = [UIImage imageNamed:@"day.png"];
        [NDMode setBackgroundImage:btnImage forState:UIControlStateNormal];
        
    }
    else{
        self.view.backgroundColor = [UIColor whiteColor];
        [GameImageV setBackgroundColor:[UIColor whiteColor]];
        UIImage *image = [UIImage imageNamed: @"bg.png"];
        [GameImageV setImage:image];

        NDMode.tag=0;
        UIImage *btnImage = [UIImage imageNamed:@"night.png"];
        [NDMode setBackgroundImage:btnImage forState:UIControlStateNormal];
        
        
    }
    
}

-(void)randomgetblocks{//
    movecountx = 5;
    movecounty = 0;
    rotationcount = 0;
    isbottom = NO;
    
    switch (nowblock) {
        case 0:
            oneblock = [allblock get_block_blue];
            break;
        case 1:
            oneblock = [allblock get_block_orange];
            break;
        case 2:
            oneblock = [allblock get_block_skyblue];
            break;
        case 3:
            oneblock = [allblock get_block_green];
            break;
        case 4:
            oneblock = [allblock get_block_purple];
            break;
        case 5:
            oneblock = [allblock get_block_yellow];
            break;
        case 6:
            oneblock = [allblock get_block_red];
            break;
        default:
            break;
    }
    blockspos = [oneblock.block_rotation objectAtIndex:rotationcount];
    
    [oneshapearr removeAllObjects];
    for (k = 0; k < oneblock.block_num; k++) {
        imgview = [[UIImageView alloc]init];
        [imgview setImage:[UIImage imageNamed:[blockpicarr objectAtIndex:nowblock]]];
        [oneshapearr addObject:imgview];
        [self.view addSubview:[oneshapearr objectAtIndex:k]];
    }
    [self performSelector:@selector(changeblockpos)];
    
    nextblock = (arc4random() % (kindnum)) + 0;
    switch (nextblock) {
        case 0:
            nextoneblock = [allblock get_block_blue];
            break;
        case 1:
            nextoneblock = [allblock get_block_orange];
            break;
        case 2:
            nextoneblock = [allblock get_block_skyblue];
            break;
        case 3:
            nextoneblock = [allblock get_block_green];
            break;
        case 4:
            nextoneblock = [allblock get_block_purple];
            break;
        case 5:
            nextoneblock = [allblock get_block_yellow];
            break;
        case 6:
            nextoneblock = [allblock get_block_red];
            break;
        default:
            break;
    }
    nextblockspos = [nextoneblock.block_rotation objectAtIndex:0];
    for (k = 0; k < nextoneblock.block_num; k++) {
        if (k >= nextoneblock.block_num) {
            [[nextshapearr objectAtIndex:k] setImage:Nil];
        }else{
            i = [[nextblockspos substringWithRange:NSMakeRange(k * 4, 1)] intValue];
            j = [[nextblockspos substringWithRange:NSMakeRange(k * 4 + 2, 1)] intValue];
            [[nextshapearr objectAtIndex:k] setImage:[UIImage imageNamed:[blockpicarr objectAtIndex:nextblock]]];
            [[nextshapearr objectAtIndex:k] setFrame:CGRectMake(blocksize*i, blocksize*j, blocksize, blocksize)];
        }
    }
    nowblock = nextblock;
}

-(BOOL)checkblockspos{
    for (k = 0; k < oneblock.block_num; k++) {
        i = [[blockspos substringWithRange:NSMakeRange(k * 4, 1)] intValue];
        j = [[blockspos substringWithRange:NSMakeRange(k * 4 + 2, 1)] intValue];
        //        if ([[[wholearr objectAtIndex:movecounty+j] objectAtIndex:movecountx+i] isEqualToString:@"0"]) {
        //
        //        }else{
        //            return NO;
        //        }
        if ([[[wholearr objectAtIndex:movecounty+j] objectAtIndex:movecountx+i] isEqual:@"0"]) {
        }else{
            return NO;
        }
    }
    return YES;
}

-(void)moveblocks{//
    if (istop) {
        [timer invalidate];//
        playagain.hidden=false;
        //gameover.hidden=true;
        movecounty=0;return;
    }
    movecounty ++;
    [_dropAudio play];
    NSLog(@"movecounty=%d",movecounty);
    if (movecounty > sizey - 1 - bottommax) {
        isbottom = YES;
    }
    else{
        if([self checkblockspos]){
            
        }else{
            isbottom = YES;
        }
    }
    if (isbottom) {
        for (k = 0; k < oneblock.block_num; k++) {
            i = [[blockspos substringWithRange:NSMakeRange(k * 4, 1)] intValue];
            j = [[blockspos substringWithRange:NSMakeRange(k * 4 + 2, 1)] intValue];
            [[wholearr objectAtIndex:movecounty-1+j] replaceObjectAtIndex:movecountx+i withObject:[oneshapearr objectAtIndex:k]];
            NSLog(@"i=%d",movecounty-1+j);                 NSLog(@"j=%d",movecountx+i);
            NSLog(@"k=%d",k);

        }
        speed=0.9;
       // speed = 0.1;
        [timer invalidate];
        timer = [NSTimer scheduledTimerWithTimeInterval:speed target:self selector:@selector(moveblocks) userInfo:Nil repeats:YES];
        [self performSelector:@selector(randomgetblocks)];
        [self performSelector:@selector(removewholearrandgetscore)];
    }else{
        [self performSelector:@selector(changeblockpos)];
    }
}

-(void)changeblockpos{//
    rightmax = 0;
    bottommax = 0;
    for (k = 0; k < oneblock.block_num; k++) {
        i = [[blockspos substringWithRange:NSMakeRange(k * 4, 1)] intValue];
        j = [[blockspos substringWithRange:NSMakeRange(k * 4 + 2, 1)] intValue];
        [[oneshapearr objectAtIndex:k] setFrame:CGRectMake(startx+blocksize*(movecountx+i), starty+blocksize*(movecounty+j), blocksize, blocksize)];
        if (rightmax < i) {
            rightmax = i;
        }
        if (bottommax < j) {
            bottommax = j;
        }
    }
}

-(IBAction)rotationblocks:(id)sender{//
    rotationcount ++;
    [_rotateaudio play];
    if (rotationcount > [oneblock.block_rotation count]-1) {
        rotationcount = 0;
    }
    blockspos = [oneblock.block_rotation objectAtIndex:rotationcount];
    [self performSelector:@selector(changeblockpos)];
    if (movecountx > (sizex - rightmax - 1)) {//
        movecountx = sizex - rightmax - 1;
    }
    if (movecounty > sizey - bottommax - 1) {//
        movecounty = sizey - bottommax - 1;
    }
    for (k = 0; k < oneblock.block_num; k++) {//
        i = [[blockspos substringWithRange:NSMakeRange(k * 4, 1)] intValue];
        j = [[blockspos substringWithRange:NSMakeRange(k * 4 + 2, 1)] intValue];
        if ([[[wholearr objectAtIndex:movecounty+j] objectAtIndex:movecountx+i] isEqual:@"0"]) {
        }else{
            rotationcount --;
            if (rotationcount < 0) {
                rotationcount = [oneblock.block_rotation count]-1;
            }
            blockspos = [oneblock.block_rotation objectAtIndex:rotationcount];
            break;
        }
    }
    [self performSelector:@selector(changeblockpos)];
}

-(IBAction)leftmoveblocks:(id)sender{//
    if (movecountx > 0&&pausebtn.tag==0) {
        movecountx --;
        if([self checkblockspos]){
            [self performSelector:@selector(changeblockpos)];
        }else{
            movecountx += 1;
        }
    }
    }

-(IBAction)rightmoveblocks:(id)sender{//
   
    if (movecountx < (sizex - rightmax - 1)&&pausebtn.tag==0) {
        movecountx ++;
        if([self checkblockspos]){
            [self performSelector:@selector(changeblockpos)];
        }else{
            movecountx -= 1;
        }
    }
}

-(void)removewholearrandgetscore{//
    isfull = NO;
    hasfull = NO;
    for (j = 0; j < sizex; j++) {//
        if ([[[wholearr objectAtIndex:0] objectAtIndex:j] isEqual:@"0"]) {
        }else{
            istop = YES;//?mark
            [timer invalidate];
            playagain.hidden=false;
            
            [_gameoveraudio play];
            label.text = @"0";
            label.tag=0;
            return;
        }
        
        if ([[[wholearr objectAtIndex:1] objectAtIndex:j] isEqual:@"0"]) {
        }else{
            istop = YES;//?mark
            [timer invalidate];
            playagain.hidden=false;
            [_gameoveraudio play];

            label.text = @"0";
            label.tag=0;

            return;
        }
        
    }
    for (i = 0; i < sizey; i++) {
        for (j = 0; j < sizex; j++) {
            if ([[[wholearr objectAtIndex:i] objectAtIndex:j] isEqual:@"0"]) {
                break;//
            }else{
                if (j == sizex - 1) {//
                    isfull = YES;
                    hasfull = YES;
                    for (k = 0; k <sizex; k++) {
                        [[[wholearr objectAtIndex:i] objectAtIndex:k] removeFromSuperview];
                        [[wholearr objectAtIndex:i] replaceObjectAtIndex:k withObject:@"0"];
                    }
                    //[wholearr exchangeObjectAtIndex:i withObjectAtIndex:0];
                }
            }
        }
        if (isfull) {
            [_removeaudio play];
            
            [wholearr insertObject:[wholearr objectAtIndex:i] atIndex:0];
            [wholearr removeObjectAtIndex:i+1];
            isfull = NO;
            completenum ++;//计分
            label.text = [NSString stringWithFormat:@"%d",completenum*100];
            label.tag=completenum*100;
            if(highscore.tag<label.tag){
                highscore.text = [NSString stringWithFormat:@"%ld",(long)label.tag];
                highscore.tag=label.tag;
                
                
            }
        }
    }
    if (hasfull) {//
        for (i = 0; i < sizey; i++) {
            for (j = 0; j < sizex; j++) {
                if ([[[wholearr objectAtIndex:i] objectAtIndex:j] isEqual:@"0"]) {
                    
                }else{
                    [[[wholearr objectAtIndex:i] objectAtIndex:j] setFrame:CGRectMake(startx+blocksize*j, starty+blocksize*i, blocksize, blocksize)];
                }
            }
        }
    }
}

-(IBAction)quickspeed:(id)sender{
    NSLog(@"log4");
    if(pausebtn.tag==0){
    speed = 0.1;
    [timer invalidate];
    timer = [NSTimer scheduledTimerWithTimeInterval:speed target:self selector:@selector(moveblocks) userInfo:Nil repeats:YES];
    }
   
}
-(IBAction)nomarlspeed:(id)sender{
    if(pausebtn.tag==0){
    speed = 0.9;
    [timer invalidate];
    timer = [NSTimer scheduledTimerWithTimeInterval:speed target:self selector:@selector(moveblocks) userInfo:Nil repeats:YES];
    }
}
-(IBAction)pauseandresume:(id)sender{
    //UIImage *btnImage = [UIImage imageNamed:@"image.png"];
    //[btnTwo setImage:btnImage forState:UIControlStateNormal];
    
    [_groudaudio play];
    if (pausebtn.tag==0){
        [timer setFireDate:[NSDate distantFuture]];
        pausebtn.tag=1;
        UIImage *btnImage = [UIImage imageNamed:@"P.png"];
        [pausebtn setBackgroundImage:btnImage forState:UIControlStateNormal];
        
    }else{
        [timer setFireDate:[NSDate date]];
        UIImage *btnImage = [UIImage imageNamed:@"Pau.png"];
        [pausebtn setBackgroundImage:btnImage forState:UIControlStateNormal];
        pausebtn.tag=0;
    }
}

- (IBAction)redoGame:(id)sender {
   // gameover.hidden =true;
    
    [_audioPlayer play];

    // NSLog(@"log3");
    movecounty=0;
    // int i,j;
   i=0;j=0;
     for(i=0;i<sizey;i++) {//
         for (j = 0; j < sizex; j++){
             if (![[[wholearr objectAtIndex:i] objectAtIndex:j] isEqual:@"0"]) {
            // }else{
                 NSLog(@"i=%d",i);                 NSLog(@"j=%d",j);

                 
                   [[[wholearr objectAtIndex:i] objectAtIndex:j] removeFromSuperview];
                    [[wholearr objectAtIndex:i] replaceObjectAtIndex:j withObject:@"0"];
            //
             }
           
        }
    }
    UIImage *btnImage = [UIImage imageNamed:@"Pau.png"];
    [pausebtn setBackgroundImage:btnImage forState:UIControlStateNormal];
    pausebtn.tag=0;

    //NSLog(@"log");
    istop = NO;
     speed = 0.9;
    timer = [NSTimer scheduledTimerWithTimeInterval:speed target:self selector:@selector(moveblocks) userInfo:Nil repeats:YES];
    //timer = [NSTimer scheduledTimerWithTimeInterval:speed target:self selector:@selector(moveblocks) userInfo:Nil repeats:YES];
    playagain.hidden=true;
    
}



- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}


@end
