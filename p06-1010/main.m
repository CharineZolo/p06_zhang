//
//  main.m
//  p06-1010
//
//  Created by 米拉 on 4/6/17.
//  Copyright © 2017 米拉. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
