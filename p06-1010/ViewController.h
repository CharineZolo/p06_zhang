//
//  ViewController.h
//  p06-1010
//
//  Created by 米拉 on 4/6/17.
//  Copyright © 2017 米拉. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "blocks.h"
#import "block.h"

@interface ViewController : UIViewController{
IBOutlet UIView *nextblockview;
IBOutlet UILabel *label;
IBOutlet UIButton *pausebtn;
blocks *allblock;
block *oneblock;
block *nextoneblock;

//IBOutlet UIButton *playagain;

int completenum;
NSTimeInterval speed;
NSTimer *timer;
NSArray *blockpicarr;
NSMutableArray *blockimgarr;
int kindnum;

int i;
int j;
int k;
int sizex;
int sizey;
NSMutableArray *wholearr;
int movecountx;
int movecounty;
int startx;
int starty;
NSMutableArray *oneshapearr;
NSMutableArray *nextshapearr;
NSString *blockspos;
NSString *nextblockspos;
int blocksize;
int maxnum;

int nowblock;
int nextblock;
UIImageView *imgview;
int rotationcount;
int rightmax;
int bottommax;
BOOL isbottom;
BOOL hasfull;
BOOL isfull;
BOOL istop;
}

@property (strong, nonatomic) IBOutlet UIImageView *GameImageV;
@property (strong, nonatomic) IBOutlet UILabel *highscore;
@property(nonatomic,retain)blocks *allblock;
@property(nonatomic,retain)block *oneblock;
@property(assign)NSTimeInterval speed;
//@property (nonatomic,strong)IBOutlet UILabel *gameover;
@property (nonatomic,strong)IBOutlet UIButton *playagain;
@property (strong, nonatomic) IBOutlet UIButton *NDMode;

-(IBAction)rotationblocks:(id)sender;
-(IBAction)rightmoveblocks:(id)sender;
-(IBAction)leftmoveblocks:(id)sender;

-(IBAction)quickspeed:(id)sender;
-(IBAction)nomarlspeed:(id)sender;
-(IBAction)pauseandresume:(id)sender;



@end

