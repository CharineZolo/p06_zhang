//
//  AppDelegate.h
//  p06-1010
//
//  Created by 米拉 on 4/6/17.
//  Copyright © 2017 米拉. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

